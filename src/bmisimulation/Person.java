/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package bmisimulation;

/**
 *
 * @author DHAIRYA
 */
public class Person {
    private double weight;
    private double height;
    private String name;
    private Weight unitWeight;
    private Height unitHeight;

    public Person(double weight, double height, String name, Weight unitWeight, Height unitHeight) {
        this.weight = weight;
        this.height = height;
        this.name = name;
        this.unitWeight = unitWeight;
        this.unitHeight = unitHeight;
    }

    Person(String john_Smith, int i, int i0) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weight getUnitWeight() {
        return unitWeight;
    }

    public void setUnitWeight(Weight unitWeight) {
        this.unitWeight = unitWeight;
    }

    public Height getUnitHeight() {
        return unitHeight;
    }

    public void setUnitHeight(Height unitHeight) {
        this.unitHeight = unitHeight;
    }
    
    public double getBMI(){
        double weightk = 0;
        double heightm = 0;
        
        switch(unitWeight)
        {
            case K: weightk = weight;
            break;
            case LB: weightk = weight * 0.4535;
            break;
            
        }
        
        switch(unitHeight)
        {
            case M: heightm = height;
            break;
            case IN: heightm = height * 0.254;
            break;
        }
        return weightk;
        
    }

    public Person(Weight unitWeight, Height unitHeight) {
        this.unitWeight = unitWeight;
        this.unitHeight = unitHeight;
    }
    
    
    
    
    
}
