/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package bmisimulation;

/**
 *
 * @author DHAIRYA
 */
public class Student extends Person
{
    private String name;
    private String rollno;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRollno() {
        return rollno;
    }

    public void setRollno(String rollno) {
        this.rollno = rollno;
    }

    public Student(String rollno) {
        this.rollno = rollno;
    }

    public Student(String name, String john_Smith, int i, int i0) {
        super(john_Smith, i, i0);
        this.name = name;
    }

    public Student(String name, Weight unitWeight, Height unitHeight) {
        super(unitWeight, unitHeight);
        this.name = name;
    }
    
}
