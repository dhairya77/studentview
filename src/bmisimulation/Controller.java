/*
 * Dhairya Kachhia.
 * Student ID : 991620361
 * Subject -
 */
package bmisimulation;

/**
 *
 * @author DHAIRYA
 */
public class Controller {
    
    private Student model;
    private Studentview view;

    public Controller(Student model, Studentview view) {
        this.model = model;
        this.view = view;
    }

    public Student getModel() {
        return model;
    }

    public void setModel(Student model) {
        this.model = model;
    }

    public Studentview getView() {
        return view;
    }

    public void setView(Studentview view) {
        this.view = view;
    }
    
    
}
